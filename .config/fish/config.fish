if not status is-login
    if status is-interactive
        # Commands to run in interactive sessions can go here
        neofetch
        starship init fish | source
    end
end

function fish_greeting

end

set -gx JAVA_HOME /usr/lib/jvm/java-17-openjdk

set NPM_PACKAGES "$HOME/.npm-packages"
set PATH $PATH $NPM_PACKAGES/bin
set MANPATH $NPM_PACKAGES/share/man $MANPATH

alias config='/usr/bin/git --git-dir=$HOME/dotfiles/ --work-tree=$HOME'

fish_add_path ~/.npm-packages/bin
fish_add_path ~/.local/bin
